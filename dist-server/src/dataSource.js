"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.observeExternalSourceChanges = observeExternalSourceChanges;
var internalEmitter = null;
var timer = null;
var entityParameters = {
  "ID": {
    type: 'string'
  },
  "Параметр 1": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 2": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 3": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 4": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 5": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 6": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 7": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 8": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 9": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 10": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 11": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 12": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 13": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 14": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 15": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 16": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 17": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 18": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 19": {
    type: 'double',
    min: -1,
    max: 1
  },
  "Параметр 20": {
    type: 'double',
    min: -1,
    max: 1
  }
};

function observeExternalSourceChanges(eventEmitter) {
  internalEmitter = eventEmitter;
  timer = setInterval(function () {
    var entities = updateEntities();
    eventEmitter.emit('update-data', entities);
    disableSourceTry();
  }, 1000);
}

var entities = [];

function createEntities() {
  for (var i = 1; i <= 20; i++) {
    var entity = {};

    for (var param in entityParameters) {
      var paramProperties = entityParameters[param];

      if (paramProperties.type === 'string') {
        entity[param] = 'Entity' + i;
      } else if (paramProperties.type === 'double') {
        entity[param] = generateRandomDecimal(paramProperties.min, paramProperties.max);
      }
    }

    entities.push(entity);
  }

  shuffleArray(entities);
  return entities;
}

function updateEntities() {
  for (var entity in entities) {
    for (var param in entityParameters) {
      var paramProperties = entityParameters[param];
      var shouldBeChanged = Math.random() > 0.5;

      if (paramProperties.type === 'double' && shouldBeChanged) {
        entities[entity][param] = generateRandomDecimal(paramProperties.min, paramProperties.max);
      }
    }
  }

  shuffleArray(entities);
  return entities;
}

function generateRandomDecimal(min, max) {
  var precision = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;
  return (Math.random() * (max - min) + min).toFixed(precision);
}

function generateRandomInteger(min, max) {
  var rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

function disableSourceTry() {
  var timeout = generateRandomInteger(1, 10);

  if (timeout <= 5) {
    return;
  }

  clearInterval(timer);
  console.log("Mock data source will not be available for a ".concat(timeout, " seconds."));
  var disableTimer = null;
  disableTimer = setTimeout(function () {
    console.log('Mock data source is available now.');
    clearTimeout(disableTimer);
    observeExternalSourceChanges(internalEmitter);
  }, timeout * 1000);
}

function generateRandomString() {
  var dt = new Date().getTime();
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c === 'x' ? r : r & 0x3 | 0x8).toString(16);
  });
}

function shuffleArray(array) {
  //Fisher-Yets shuffle algorithm
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var _ref = [array[j], array[i]];
    array[i] = _ref[0];
    array[j] = _ref[1];
  }
}

createEntities();
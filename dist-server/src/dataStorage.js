"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.latestEntity = latestEntity;
exports.allEntities = allEntities;

var events = _interopRequireWildcard(require("events"));

var _dataSource = require("./dataSource");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var inMemoryStorage = [];
var sourceEventEmitter = new events.EventEmitter();

function latestEntity() {
  if (inMemoryStorage.length === 0) {
    return null;
  }

  return inMemoryStorage.slice(-1);
}

function allEntities() {
  if (inMemoryStorage.length === 0) {
    return [];
  }

  return inMemoryStorage;
}

(0, _dataSource.observeExternalSourceChanges)(sourceEventEmitter);
sourceEventEmitter.on('update-data', function (data) {
  inMemoryStorage = data;
});
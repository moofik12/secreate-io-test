import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import allEntities from './routes/allEntities';
import latestEntity from './routes/latestEntity';
import cors from 'cors';

let app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use('/api/entities', allEntities);
app.use('/api/entities/latest', latestEntity);

export default app;

import {latestEntity} from "../src/dataStorage";
import express from 'express';

let router = express.Router();

router.get('/', function(req, res, next) {
  res.json(latestEntity());
});

export default router;
let internalEmitter = null;
let timer = null;
const entityParameters = {
    "ID": {type: 'string'},
    "Параметр 1": {type: 'double', min: -1, max: 1},
    "Параметр 2": {type: 'double', min: -1, max: 1},
    "Параметр 3": {type: 'double', min: -1, max: 1},
    "Параметр 4": {type: 'double', min: -1, max: 1},
    "Параметр 5": {type: 'double', min: -1, max: 1},
    "Параметр 6": {type: 'double', min: -1, max: 1},
    "Параметр 7": {type: 'double', min: -1, max: 1},
    "Параметр 8": {type: 'double', min: -1, max: 1},
    "Параметр 9": {type: 'double', min: -1, max: 1},
    "Параметр 10": {type: 'double', min: -1, max: 1},
    "Параметр 11": {type: 'double', min: -1, max: 1},
    "Параметр 12": {type: 'double', min: -1, max: 1},
    "Параметр 13": {type: 'double', min: -1, max: 1},
    "Параметр 14": {type: 'double', min: -1, max: 1},
    "Параметр 15": {type: 'double', min: -1, max: 1},
    "Параметр 16": {type: 'double', min: -1, max: 1},
    "Параметр 17": {type: 'double', min: -1, max: 1},
    "Параметр 18": {type: 'double', min: -1, max: 1},
    "Параметр 19": {type: 'double', min: -1, max: 1},
    "Параметр 20": {type: 'double', min: -1, max: 1},
};



export function observeExternalSourceChanges(eventEmitter) {
    internalEmitter = eventEmitter;
    timer = setInterval(() => {
        let entities = updateEntities();
        eventEmitter.emit('update-data', entities);
        disableSourceTry();
    }, 1000);
}

let entities = [];

function createEntities() {
    for (let i = 1; i <= 20; i++) {
        let entity = {};

        for (let param in entityParameters) {
            let paramProperties = entityParameters[param];

            if (paramProperties.type === 'string') {
                entity[param] = 'Entity' + i;
            } else if (paramProperties.type === 'double') {
                entity[param] = generateRandomDecimal(paramProperties.min, paramProperties.max);
            }
        }

        entities.push(entity);
    }

    shuffleArray(entities);

    return entities;
}

function updateEntities() {
    for (let entity in entities) {
        for (let param in entityParameters) {
            let paramProperties = entityParameters[param];

            let shouldBeChanged = (Math.random() > 0.5);

            if (paramProperties.type === 'double' && shouldBeChanged) {
                entities[entity][param] = generateRandomDecimal(paramProperties.min, paramProperties.max);
            }
        }
    }

    shuffleArray(entities);

    return entities;
}

function generateRandomDecimal(min, max, precision = 3) {
    return (Math.random() * (max - min) + min).toFixed(precision);
}

function generateRandomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

function disableSourceTry() {
    let timeout = generateRandomInteger(1, 10);
    if (timeout <= 5) {
        return;
    }

    clearInterval(timer);
    console.log(`Mock data source will not be available for a ${timeout} seconds.`);

    let disableTimer = null;
    disableTimer = setTimeout(() => {
        console.log('Mock data source is available now.');
        clearTimeout(disableTimer);
        observeExternalSourceChanges(internalEmitter);
    }, timeout * 1000)
}

function shuffleArray(array) {
    //Fisher-Yets shuffle algorithm
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

createEntities();
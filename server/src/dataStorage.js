import * as events from "events";
import { observeExternalSourceChanges } from "./dataSource";

let inMemoryStorage = [];
const sourceEventEmitter = new events.EventEmitter();

export function latestEntity() {
    if (inMemoryStorage.length === 0) {
        return null;
    }

    return inMemoryStorage.slice(-1);
}

export function allEntities() {
    if (inMemoryStorage.length === 0) {
        return [];
    }

    return inMemoryStorage;
}

observeExternalSourceChanges(sourceEventEmitter);

sourceEventEmitter.on('update-data', function (data) {
    inMemoryStorage = data;
});